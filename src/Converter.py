# ISBN CONVERTER
# CHALLENGE CODE FROM RAKUTEN SINGAPORE

# BRYAN CHRISTOVAL KIANJAYA
# 14 - 04 - 2019

class ConversionException(Exception):

    # Method to differentiate error possibility during the conversion

    def __init__(self, message):
        Exception.__init__(self, message)

    @staticmethod
    def for_invalid_id_type():
        return ConversionException("Invalid product ID type")

    @staticmethod
    def for_invalid_id_length():
        return ConversionException("Invalid product ID length")


class IsbnConverter(object):

    _ISBN_10_LENGTH = 10  # Standard and accepted length of ISBN-10 number.
    _PRODUCT_ID_LENGTH = 12  # Standard and accepted length of Product ID

    def convert_from_product_id(self, product_id):

        # 1. Remove the first 3 digits of product ID
        # 2.  Remaining digits are ISBN-10 number without error control
        # 3. Error control is computed for the final ISBN number

        self.__validate_product_id(product_id)
        partial_isbn = product_id[3:]
        error_control = self.__calc_error_control(partial_isbn)

        return partial_isbn + error_control

    def __calc_error_control(self, partial_isbn):

        # Calculate error control for ISBN-10 numbers
        # The last digit of ISBN-10 number provides an error detection
        # Once the entire 10 digit of ISBN-10 are successfully converted, it will be checked for errors whenever is it is transcribed
        # The error control digits is calculated considering the weighted sum of the first 9 digits
        # and will produced a number from a remainder of thw weighted sum divided by 11
        # Letter 'x' is used to replace when 10 is the required digit

        weight_sum = 0
        for i, digit in enumerate(partial_isbn):
            weight_sum += (self._ISBN_10_LENGTH - i) * int(digit)  # Weighted sum of digits

        weighted_sum_mod_11 = (weight_sum % 11)
        if weighted_sum_mod_11 == 0:
            control = str(weighted_sum_mod_11)  # Not reminder so control should be zero
        else:
            c_digit = 11 - weighted_sum_mod_11
            control = str(c_digit) if c_digit < self._ISBN_10_LENGTH else "x"  # Replace by 'x' when 10 is required as digit

        return control

    def __validate_product_id(self, product_id):

        # Last check to validity the product ID
        # 1. Can be converted to Integer
        # 2. Has valid length

        try:
            int(product_id)
        except ValueError:
            raise ConversionException.for_invalid_id_type()

        if len(product_id) is not self._PRODUCT_ID_LENGTH:
            raise ConversionException.for_invalid_id_length()
# ISBN CONVERTER
# CHALLENGE CODE FROM RAKUTEN SINGAPORE

# BRYAN CHRISTOVAL KIANJAYA
# 14/04/2019

import unittest
from Converter import IsbnConverter
from Converter import ConversionException


class IsbnConverterTests(unittest.TestCase):  # Test case for ISBNConverter class business logic.

    def setUp(self):  # Setup converter

        self.subject = IsbnConverter()

    def test_invalid_id_length(self):  # Test if product ID length is more than it supposed to be

        oversized_id = "9781551923709"
        self.assertRaises(ConversionException,
                          self.subject.convert_from_product_id,
                          oversized_id)

        undersized_id = "978155192"
        self.assertRaises(ConversionException,
                          self.subject.convert_from_product_id,
                          undersized_id)

    def test_invalid_id_type(self):  # Test if invalid product ID type is identified

        invalid_id_as_string = "na"
        self.assertRaises(ConversionException,
                          self.subject.convert_from_product_id,
                          invalid_id_as_string)

        invalid_id_as_float = "9781551.9237"
        self.assertRaises(ConversionException,
                          self.subject.convert_from_product_id,
                          invalid_id_as_float)

        invalid_id_blended_int = "97815519237a"
        self.assertRaises(ConversionException,
                          self.subject.convert_from_product_id,
                          invalid_id_blended_int)

    def test_isbn_conversion_with_error_control_over_10(self):

        # Test if product ID is successfully converted to ISBN number when computed error control is over 10

        product_id_1 = "978155192370"
        expected_isbn_1 = "155192370x"
        self.assertEqual(self.subject.convert_from_product_id(product_id_1),
                         expected_isbn_1)

        product_id_2 = "978007007013"
        expected_isbn_2 = "007007013x"
        self.assertEqual(self.subject.convert_from_product_id(product_id_2),
                         expected_isbn_2)

    def test_isbn_conversion_with_error_control_under_10(self):

        # Test if product ID is successfully converted to iSBN number when computed error control is below 10

        product_id_1 = "978140007917"
        expected_isbn_1 = "1400079179"
        self.assertEqual(self.subject.convert_from_product_id(product_id_1),
                         expected_isbn_1)

        product_id_2 = "978037541457"
        expected_isbn_2 = "0375414576"
        self.assertEqual(self.subject.convert_from_product_id(product_id_2),
                         expected_isbn_2)


if __name__ == '__main__':
    unittest.main()
# ISBN CONVERTER
# CHALLENGE CODE FROM RAKUTEN SINGAPORE

# BRYAN CHRISTOVAL KIANJAYA
# 14 - 04 - 2019

# Product ID converter to produce standard ISBN-10 number
# This file transform a product ID to ISBN-10 number
# also handle the input from STDIN and display the result from STDOUT


from src import Converter

PRODUCT_ID_INPUT_TEXT = "Enter the product ID: "
ISBN_RESULT_TEXT = "ISBN-10 Number: %s"
ISBN_ERROR_TEXT = "An error has occurred: %s"


def execute_conversion():

    # Input from STDIN and display the result on STDOUT
    # If the conversion does not succeed, an error message will printed on STDOUT
    # Example:

    # >>> execute_conversion()
    # Enter the product ID: 978155192370
    # ISBN-10 number: 155192370x
    # >>> execute_conversion()
    # Enter the product I: na
    # An error has occurred: Invalid product ID

    isbn_converter = Converter.IsbnConverter()
    product_id = raw_input(PRODUCT_ID_INPUT_TEXT)
    result_text = ""

    try:
        isbn = isbn_converter.convert_from_product_id(product_id)
        result_text = ISBN_RESULT_TEXT % isbn
    except Converter.ConversionException, e:
        result_text = ISBN_ERROR_TEXT % e.message

    print result_text


if __name__ == '__main__':
    execute_conversion()